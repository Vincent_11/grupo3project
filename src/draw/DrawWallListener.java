package draw;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import Manager.DevelopmentManager;
import main.Display;
import main.WallMenuFrame;

public class DrawWallListener extends MouseAdapter {

    private Display panel;

    public DrawWallListener(Display panel) {
        super();
        this.panel= panel;
    }

    /** @author Vincent Prado
     * @date 3/4/2020
     * @param e Sets wall coordinates based on cursor position. */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (DevelopmentManager.isCreatingBuilding() || DevelopmentManager.isCreatingTree()) {
            if (DevelopmentManager.isFirstCoordinate()) {// Se va a colocar el primer punto de una
                                                         // pared.
                DevelopmentManager.setFirstXCoordinate(e.getX());
                DevelopmentManager.setFirstYCoordinate(e.getY());
                DevelopmentManager.setCoordinateInTextFields(Integer.toString(e.getX()),
                    Integer.toString(e.getY()));
                if (DevelopmentManager.isCreatingBuilding())
                    DevelopmentManager.setIsFirstCoordinate(false);
                else
                    DevelopmentManager.addTree();
            } else {
                DevelopmentManager.setSecondXCoordinate(e.getX());
                DevelopmentManager.setSecondYCoordinate(e.getY());
                DevelopmentManager.setCoordinateInTextFields(Integer.toString(e.getX()),
                    Integer.toString(e.getY()));

                // a new frame is created as a wall creator menu Author: Emmanuel
                WallMenuFrame wallCreator= new WallMenuFrame();
                wallCreator.setDisplay(panel);
                wallCreator.setVisible(true);

                DevelopmentManager.setIsFirstCoordinate(true);
            }
        }
    }

    /** @author Vincent Prado
     * @param e
     * @date 3/4/2020 Changes label with cursor position when cursor is moved and sets new
     *       coordinates for helper line when cursor is moved. */
    @Override
    public void mouseMoved(MouseEvent e) {
        DevelopmentManager.getDisplay().setCursorPosition(e.getX(), e.getY());
        if (!DevelopmentManager.isFirstCoordinate()) {
            DevelopmentManager.setxHelper(e.getX());
            DevelopmentManager.setyHelper(e.getY());
        }
    }

}