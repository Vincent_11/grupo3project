package draw;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import Manager.GameManager;
import main.Display;

public class KeyManager implements ActionListener, KeyListener {

    private static Display display;

    public KeyManager(Display display) {
        KeyManager.display= display;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code= e.getKeyCode();
        if (code == KeyEvent.VK_UP) {
            GameManager.up();
        }
        if (code == KeyEvent.VK_DOWN) {
            GameManager.down();
        }
        if (code == KeyEvent.VK_RIGHT) {
            GameManager.right();
        }
        if (code == KeyEvent.VK_LEFT) {
            GameManager.left();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        display.repaint();
    }
}
