package Manager;

import java.awt.Graphics;
import java.io.IOException;

import game_objects.Building;
import game_objects.Player;
import main.CollisionDetector;
import main.Display;
import main.GameTaskBar;

public class GameManager extends Manager {

    private static CollisionDetector collisionDetector;
    private static int xPosition= 100;
    private static int yPosition= 650;
    private static Player player= new Player(GameManager.xPosition, GameManager.yPosition);

    private static final int PLAYER_SPEED= 3;

    public GameManager(Display display, GameTaskBar gameTaskBar) {
        super(display, gameTaskBar);
        collisionDetector= new CollisionDetector(player);
    }

    /** @author Vincent Prado
     * Moves the player up. */
    public static void up() {
        if (player.getPlayerHitbox().getMinY() <= 0) {
            return;
        } else {
            player.setyPosition(player.getyPosition() - PLAYER_SPEED);
            collisionDetector.buildingProximityDetector(player, buildings);

            display.repaint();
        }
    }

    /** @author Vincent Prado
     * Moves the player down. */
    public static void down() {
        if (player.getPlayerHitbox().getMaxY() >= display.getHeight()) {
            return;
        } else {
            player.setyPosition(player.getyPosition() + PLAYER_SPEED);
            collisionDetector.buildingProximityDetector(player, buildings);

            display.repaint();
        }
    }

    /** @author Vincent Prado
     * Moves to the right. */
    public static void right() {
        if (player.getPlayerHitbox().getMaxX() >= display.getWidth()) {
            return;
        } else {
            player.setxPosition(player.getxPosition() + PLAYER_SPEED);
            collisionDetector.buildingProximityDetector(player, buildings);

            display.repaint();
        }
    }

    /** @author Vincent Prado
     * Moves to the left. */
    public static void left() {
        if (player.getPlayerHitbox().getMinX() <= 0) {
            return;
        } else {
            player.setxPosition(player.getxPosition() - PLAYER_SPEED);
            collisionDetector.buildingProximityDetector(player, buildings);

            display.repaint();
        }
    }

    /** @author Vincent Prado
     * @param g graphic context
     * This method will draw everything concerning the game state of the
     * program */
    @Override
    public void draw(Graphics g)throws IOException {
            super.draw(g);
        player.draw(g);

        // draws the perimetor of any building that the player is close to
        for (Building b : buildings) {
            if (b.isClose() == true) {
                g.drawRect((int) b.getHitbox().getMinX(),
                    (int) b.getHitbox().getY(),
                    (int) b.getHitbox().getWidth(),
                    (int) b.getHitbox().getHeight());
            }
        }
    }

    public static CollisionDetector getCollisionDetector() {
        return collisionDetector;
    }

}
