package Manager;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JPanel;

import game_objects.Building;
import game_objects.Question;
import game_objects.Tree;
import game_objects.Wall;
import main.Display;

/** This class has not been finished. */
public class Manager {

    protected static Display display;
    protected static JPanel taskBar;

    protected static ArrayList<Building> buildings;
    protected static ArrayList<Tree> trees;
    protected static ArrayList<Wall> walls;// Use to store all walls
    protected static ArrayList<Question> questions;
    protected static ArrayList<Wall> allWalls;

    protected static String wallpaperDirectory;
    protected static BufferedImage backgroundImage;

    public Manager(Display display, JPanel taskBar) {
        this.display= display;
        this.taskBar= taskBar;
        buildings= new ArrayList<>();
        trees= new ArrayList<>();
        walls= new ArrayList<>();
        allWalls= new ArrayList<>();
        questions= new ArrayList<>();
    }

    public static Manager currentState= null;

    public Manager() {

    }

    public static Manager getCurrentState() {
        return currentState;
    }

    public static void setCurrentState(Manager currentState) {
        Manager.currentState= currentState;
    }

    /** @author: Vincent Prado
     * @throws IOException
     * @date 3/4/2020 Draw main objects */
    protected void draw(Graphics g) throws IOException {
        g.setColor(Color.BLACK);
        g.drawImage(backgroundImage, 0, 0, 1366, 768, null); // 0, 0, frame width, frame height -
        // taskbar height

        // draws the 2D image for all buildings
        for (Building b : buildings) {
            if (b.isVisible()) {
                BufferedImage image= b.getBuilding2DImage();
                int x= b.getUpperLeftPoint().getX();
                int y= b.getUpperLeftPoint().getY();
                int width= b.getBottomRightPoint().getX() - b.getUpperLeftPoint().getX();
                int height= b.getBottomRightPoint().getY() - b.getUpperLeftPoint().getY();
                g.drawImage(image, x, y, width, height, null);
            }
        }

        for (Tree t : trees) {
            t.draw(g, t.getPosition().getX() - 25, t.getPosition().getY() - 25, 50, 50);
        }

    }
}
