package Manager;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import game_objects.Building;
import game_objects.BuildingFinishingTouches;
import game_objects.Wall;
import main.Display;
import main.GameTaskBar;
import main.TaskBar;

public class StateChangerManager extends Manager {

    private static DevelopmentManager developmentManager;
    private static GameManager gameManager;
    private static Manager currentManager;
    private static GameTaskBar gameTaskBar;
    private static TaskBar developmentTaskBar;
    private static JPanel currentTaskBar;
    private static JFrame frame;
    private static Display display;

    public StateChangerManager(int width, int height, JFrame frame) {
        super();
        this.frame= frame;
        gameTaskBar= new GameTaskBar();
        developmentTaskBar= new TaskBar();
        this.frame.add(developmentTaskBar, BorderLayout.SOUTH);
        display= new Display(width, height);
        this.frame.add(display, BorderLayout.NORTH);
        BuildingFinishingTouches bft= new BuildingFinishingTouches();
        gameManager= new GameManager(display, gameTaskBar);
        developmentManager= new DevelopmentManager(display, developmentTaskBar, bft);
        currentManager= developmentManager;
        currentTaskBar= developmentTaskBar;
        frame.pack();
        developmentManager.backgroundImageSetter(); // launches backgroundImage JFileChooser
        display.repaint();
    }

    /**
     * @auhor Vincent Prado
     * @date 4/9/2020
     * This method changes the state from development mode to game mode
     */
    public static void changeToGameMode() {
        setBuildingVisibility(false);
        setWallVisibility(false);
        currentManager= gameManager;
        display.repaint();
        changeTaskBar();

        gameManager.getCollisionDetector().setBuildingHitboxes(buildings); // sets the hitbox for
                                                                           // any building that
                                                                           // doesn't have one
    }

    /**
     * @auhor Vincent Prado
     * @date 4/9/2020
     * This method changes the state from game mode to development mode
     */
    public static void changeToDevelopmentMode() {
        setBuildingVisibility(true);
        setWallVisibility(true);
        currentManager= developmentManager;
        display.repaint();
        changeTaskBar();
    }

    /**
     * @auhor Vincent Prado
     * @date 4/9/2020
     * This method is use to determine if a building should be visible or not.
     */
    private static void setBuildingVisibility(boolean visibility) {
        for (Building b : buildings) {
            b.setVisible(visibility);
        }
    }

    /**
     * @auhor Vincent Prado
     * @date 4/9/2020
     * This method is use to determine if a wall should be visible or not.
     */
    private static void setWallVisibility(boolean visibility) {
        for (Wall w : allWalls) {
            w.setVisible(visibility);
        }
    }

    /**
     * @auhor Vincent Prado
     * @date 4/9/2020
     * This method is use to change the task bar look depending on which mode (development or game) the user is in.
     */
    private static void changeTaskBar() {
        if (currentTaskBar == developmentTaskBar) {
            frame.getContentPane().remove(developmentTaskBar);
            frame.add(gameTaskBar, BorderLayout.SOUTH);
            frame.getContentPane().invalidate();
            frame.getContentPane().validate();
            currentTaskBar= gameTaskBar;
            gameTaskBar.repaint();
        } else {
            frame.getContentPane().remove(gameTaskBar);
            frame.add(developmentTaskBar, BorderLayout.SOUTH);
            frame.getContentPane().invalidate();
            frame.getContentPane().validate();
            currentTaskBar= developmentTaskBar;
            developmentTaskBar.repaint();
        }
    }

    /**
     * @author Vincent Prado
     * @param g
     * @throws IOException
     * This method calls the method draw from the current manger been use (either DevelopmentManager or GameManager).
     */
    public static void drawState(Graphics g)throws IOException {
            currentManager.draw(g);
    }

}
