package Manager;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import game_objects.Building;
import game_objects.BuildingFinishingTouches;
import game_objects.Question;
import game_objects.Tree;
import game_objects.Wall;
import game_objects.trees.Tree1;
import game_objects.trees.Tree2;
import game_objects.trees.Tree3;
import main.Display;
import main.TaskBar;

public class DevelopmentManager extends Manager {

    private static BuildingFinishingTouches bft;
    private static boolean isCreatingBuilding;
    private static boolean finishedBuilding;
    private static boolean isCreatingTree;
    private static boolean isTree1;
    private static boolean isTree2;
    private static boolean isTree3;

    private static int firstXCoordinate, firstYCoordinate, secondXCoordinate, secondYCoordinate; // wall
    // coordinates
    private static boolean isFirstCoordinate= true;// set to true when user is going to set the
    // first coordinate of a wall
    private static int xHelper, yHelper;// Use to draw helper line
    private static int wallHeight;

    public DevelopmentManager(Display display, TaskBar taskBar, BuildingFinishingTouches bft) {
        super(display, taskBar);
        this.bft= bft;
        isCreatingBuilding= false;
        finishedBuilding= false;
        isCreatingTree= false;
        isTree1= false;
        isTree2= false;
        isTree3= false;
        buildings= new ArrayList<>();
        trees= new ArrayList<>();
        walls= new ArrayList<>();
        allWalls= new ArrayList<>();
        questions= new ArrayList<>();
    }

    /** @author Vincent Prado
     * @date 3/6/2020
     * @param x is the horizontal position of the cursor when the user clicks
     * @param y is the vertical position of the cursor when the user clicks This method is called
     *          every time the user clicks to display the cursor position in the text fields at the
     *          moment the user clicks. */
    public static void setCoordinateInTextFields(String x, String y) {
        ((TaskBar) taskBar).setXTextFieldValue(x);
        ((TaskBar) taskBar).setYTextFieldValue(y);
    }

    /** @author Vincent Prado
     * @date 3/6/2020 This method is called when the user wants to change the coordinates that were
     *       set by clicking. It changes the first coordinate to the one establish by the user in
     *       the text fields */
    public static void useCoordinateFromTextFields() {
        if (isCreatingBuilding) {
            if (!isFirstCoordinate) {
                firstXCoordinate= ((TaskBar) taskBar).getXTextFieldValue();
                firstYCoordinate= ((TaskBar) taskBar).getYTextFieldValue();
                display.repaint();
            }

            else {// user wants to change previous second coordinate
                Wall w= walls.get(walls.size() - 1);
                w.getCoordinate2().setX(((TaskBar) taskBar).getXTextFieldValue());
                w.getCoordinate2().setY(((TaskBar) taskBar).getYTextFieldValue());
                display.repaint();
            }
        }
    }

    /** @author: Vincent Prado
     * @date 3/4/2020 Store a walls in the array list before painting all walls. */
    public static void addWall() {
        Wall wall= new Wall(firstXCoordinate, firstYCoordinate, secondXCoordinate,
            secondYCoordinate, wallHeight,
            wallpaperDirectory);
        walls.add(wall);
        allWalls.add(wall);
        display.repaint();
    }

    public static void addWall(int x1, int y1, int x2, int y2, int height, String imagePath) {
        Wall wall= new Wall(x1, y1, x2, y2, height, imagePath);
        walls.add(wall);
        allWalls.add(wall);
        display.repaint();
    }

    /** @author: Vincent Prado
     * @date 3/12/2020 This method is called when the user presses the "Finish" button. It adds a
     *       new building object with its respective walls to the buildings array list. A new walls
     *       array list is define to store the walls for the next building. */

    public static void addBuilding(JFrame f, ArrayList<Question> q, String name) {
        if (!walls.isEmpty() && name.length() > 0 && !q.isEmpty()) {
            buildings.add(new Building(walls, q, name));

            // sets the 2DImage for the most recent building and calculates the dimensions
            building2DImageSetter(buildings.get(buildings.size() - 1));
            buildings.get(buildings.size() - 1).upperLeftPointSetter();
            buildings.get(buildings.size() - 1).bottomRightPointSetter();

            walls= new ArrayList<>();
            JOptionPane.showMessageDialog(null,
                "Congratulations, you are done creating your building. " +
                    " Please refresh your project to create coordinate text file");
        } else {
            System.out.println(walls.isEmpty());
            System.out.println(questions.isEmpty());
            System.out.println(name.length() > 0);
            System.out.println(allWalls.isEmpty());

            JOptionPane.showMessageDialog(null,
                "Please make sure a wall was created, a question was chosen or a name for the building was given.",
                "Error",
                JOptionPane.ERROR_MESSAGE);
        }
        f.dispose();
    }

    public static void addBuilding(JFrame f, ArrayList<Question> q, String name, String image) {
        if (!walls.isEmpty() && name.length() > 0 && !q.isEmpty()) {
            buildings.add(new Building(walls, q, name));

            // sets the 2DImage for the most recent building and calculates the dimensions
            building2DImageSetter(buildings.get(buildings.size() - 1));
            buildings.get(buildings.size() - 1).upperLeftPointSetter();
            buildings.get(buildings.size() - 1).bottomRightPointSetter();

            walls= new ArrayList<>();
            JOptionPane.showMessageDialog(null,
                "Congratulations, you are done creating your building. " +
                    " Please refresh your project to create coordinate text file");
        } else {
            System.out.println(walls.isEmpty());
            System.out.println(questions.isEmpty());
            System.out.println(name.length() > 0);
            System.out.println(allWalls.isEmpty());

            JOptionPane.showMessageDialog(null,
                "Please make sure a wall was created, a question was chosen or a name for the building was given.",
                "Error",
                JOptionPane.ERROR_MESSAGE);
        }
        f.dispose();
    }

    /** @author Vincent Prado This method stored trees in the trees array list so that day can later
     *         be drawn. */
    public static void addTree() {
        Tree t;
        if (isTree1)
            t= new Tree1(firstXCoordinate, firstYCoordinate);
        else if (isTree2)
            t= new Tree2(firstXCoordinate, firstYCoordinate);
        else
            t= new Tree3(firstXCoordinate, firstYCoordinate);
        trees.add(t);
        display.repaint();
    }

    public static void addTree(int coord1, int coord2, int type) {
        Tree t;
        if (type == 1)
            t= new Tree1(coord1, coord2);
        else if (type == 2)
            t= new Tree2(coord1, coord2);
        else
            t= new Tree3(coord1, coord2);
        trees.add(t);
        display.repaint();
    }

    /** @author: Vincent Prado
     * @throws IOException
     * @date 3/4/2020 Draw walls stored in the list. */
    @Override
    public void draw(Graphics g) throws IOException {
        super.draw(g);
        for (Wall w : allWalls) {
            g.drawLine(w.getCoordinate1().getX(), w.getCoordinate1().getY(),
                w.getCoordinate2().getX(), w.getCoordinate2().getY());
        }
        if (!isFirstCoordinate) {
            g.drawLine(firstXCoordinate, firstYCoordinate, xHelper, yHelper);
        }
    }

    /** @objetivo: Asignarle una imagen a backgroundImage que servir� como el fondo del mapa
     * @poscondiciones: si no hay IOExeption, se le habr� asignado valor a backgroundImage
     * @autor: Emmanuel
     * @fecha: 28/marzo/2019 */
    public void backgroundImageSetter() {
        JFileChooser backgroundImageChooser= imageChooserSetUp("Map Background");

        if (backgroundImageChooser
            .showOpenDialog(backgroundImageChooser) == JFileChooser.APPROVE_OPTION) {
            try {
                setBackgroundImage(ImageIO.read(backgroundImageChooser.getSelectedFile()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /** @objetivo: le asigna una imagen al building2DImage que se usar� para identificar el
     *            building en el mapa
     * @precondiciones: building b existe
     * @poscondiciones: si no hay IOException, se le habr� asignado valor a building2DImage
     * @param b building to which the image will be set
     * @autor: Emmanuel
     * @fecha:28/marzo/2019 */
    public static void building2DImageSetter(Building b) {
        JFileChooser building2DImageChooser= imageChooserSetUp("2D Image");

        if (building2DImageChooser
            .showOpenDialog(building2DImageChooser) == JFileChooser.APPROVE_OPTION) {
            try {
                b.setBuilding2DImage(building2DImageChooser.getSelectedFile());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /** @objetivo: crea un JFileChooser de imagenes nuevo
     * @poscondiciones: imageChooser se ha creado
     * @param title title that will show up on the file chooser
     * @autor: Emmanuel
     * @fecha:28/marzo/2019 */
    public static JFileChooser imageChooserSetUp(String title) {
        JFileChooser imageChooser= new JFileChooser(".");
        imageChooser.setVisible(true);
        imageChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileFilter imageFilter= new FileNameExtensionFilter(
            "Image files", ImageIO.getReaderFileSuffixes());
        imageChooser.setFileFilter(imageFilter);
        imageChooser.setAcceptAllFileFilterUsed(false);
        imageChooser.setDialogTitle(title);
        return imageChooser;
    }

    // -----------------------Getters and setters-----------------------------
    public static Display getDisplay() {
        return display;
    }

    public static BuildingFinishingTouches getbft() {
        return bft;
    }

    public static boolean isCreatingBuilding() {
        return isCreatingBuilding;
    }

    public static boolean isFinishedBuilding() {
        return finishedBuilding;
    }

    public static void setIsCreatingBuilding(boolean isCreatingBuilding) {
        DevelopmentManager.isCreatingBuilding= isCreatingBuilding;
    }

    public static void setFinishedBuilding(boolean finishedBuilding) {
        DevelopmentManager.finishedBuilding= finishedBuilding;
    }

    public static boolean isFirstCoordinate() {
        return isFirstCoordinate;
    }

    public static void setIsFirstCoordinate(boolean isFirstCoordinate) {
        DevelopmentManager.isFirstCoordinate= isFirstCoordinate;
    }

    public static void setFirstXCoordinate(int firstXCoordinate) {
        DevelopmentManager.firstXCoordinate= firstXCoordinate;
    }

    public static void setFirstYCoordinate(int firstYCoordinate) {
        DevelopmentManager.firstYCoordinate= firstYCoordinate;
    }

    public static void setSecondXCoordinate(int secondXCoordinate) {
        DevelopmentManager.secondXCoordinate= secondXCoordinate;
    }

    public static void setSecondYCoordinate(int secondYCoordinate) {
        DevelopmentManager.secondYCoordinate= secondYCoordinate;
    }

    public static void setxHelper(int xHelper) {
        DevelopmentManager.xHelper= xHelper;
    }

    public static void setyHelper(int yHelper) {
        DevelopmentManager.yHelper= yHelper;
    }

    public static void setWallpaperDirectory(String wallpaperDirectory) {
        DevelopmentManager.wallpaperDirectory= wallpaperDirectory;
    }

    public static void setWallHeight(int wallHeight) {
        DevelopmentManager.wallHeight= wallHeight;
    }

    public static void setIsCreatingTree(boolean isCreatingTree) {
        DevelopmentManager.isCreatingTree= isCreatingTree;
    }

    public static boolean isCreatingTree() {
        return isCreatingTree;
    }

    public static void setIsTree1(boolean isTree1) {
        DevelopmentManager.isTree1= isTree1;
    }

    public static void setIsTree2(boolean isTree2) {
        DevelopmentManager.isTree2= isTree2;
    }

    public static void setIsTree3(boolean isTree3) {
        DevelopmentManager.isTree3= isTree3;
    }

    public BufferedImage getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(BufferedImage backgroundImage) {
        this.backgroundImage= backgroundImage;
    }

    /** @objetivo: save all the critical data needed to recreate a map by just reading a text file
     * @poscondiciones: 2 new text files is generated, one for the map and one for the questions
     *                  used in map
     * @param Array of buildings, array of trees, name of map
     * @autor: Ralph Ulysse
     * @fecha:10/abril/2020 */

    public static void saveMap() throws FileNotFoundException, UnsupportedEncodingException {
        String map= JOptionPane.showInputDialog("Please Enter Map Name: ");
        PrintWriter writer= new PrintWriter("res/saves/" + map + ".txt", "UTF-8");
        PrintWriter questionWriter= new PrintWriter("res/saves/" + map + "qText.txt", "UTF-8");
        writer.write("" + map + "\n");
        for (Building x : buildings) {
            writer.write("Building name: " + x.getBuildingName() + "\n");
            writer.write("Building image: " + x.getBuildingImage().getName() + "\n");
            int wallCount= 1;
            for (Wall wall : x.getBuildingWalls()) {
                writer.write(
                    "Wall" + wallCount + ": startsAt: (" + wall.getCoordinate1().getX() + " " +
                        wall.getCoordinate1().getY() + ")," + " endsAt (" +
                        wall.getCoordinate2().getX() +
                        " " + wall.getCoordinate2().getY() + ")," + " height: " + wall.getHeight() +
                        "," + " texture Image: " + wall.getWallpaper().getName() + "\n");
                wallCount++ ;
            }

            questionWriter.write("BuildingName: " + x.getBuildingName() + "\n");
            for (Question q : x.getBuildingQuestions()) {
                questionWriter.write("Question: " + q.toString() + "\n");
                questionWriter.write(System.lineSeparator());
            }
           
        }
        for (Tree t : trees) {
            if (t instanceof Tree1) {
                writer.write("Tree type: 1, " + " location: (" +
                    t.getPosition().getX() + " " + t.getPosition().getY() + ")" + "\n");
            } else if (t instanceof Tree2) {
                writer.write("Tree type: 2, " + " location: (" +
                    t.getPosition().getX() + " " + t.getPosition().getY() + ")" + "\n");
            } else if (t instanceof Tree3) {
                writer.write("Tree type: 3, " + " location: (" +
                    t.getPosition().getX() + " " + t.getPosition().getY() + ")" + "\n");
            }
        }
        writer.write("Question File: " + map + "qText.txt" + "\n");
        writer.close();
        questionWriter.close();
        JOptionPane.showMessageDialog(null,
            "Congratulations, you are done creating your Map. " +
                " Please refresh your project to create the map's text files");
    }

}