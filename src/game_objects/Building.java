package game_objects;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import plane.Coordinate;

public class Building {

    private static final int DEFAULT_BUILDING_HEIGHT= 3;
    private Wall wall1;
    private Wall wall2;
    private Wall wall3;
    private Wall wall4;

    private Rectangle proximityBox;
    private Rectangle hitbox;
    private File building2DImage;
    private Coordinate upperLeftPoint;
    private Coordinate bottomRightPoint;
    private int height;
    private String buildingDirectory;
    private File buildpaper;

    private ArrayList<Wall> buildingWalls;
    private ArrayList<Question> buildingQuestions;
    private String buildingName;
    private boolean visible;
    private boolean close;

    public Building(ArrayList<Wall> buildingWalls, ArrayList<Question> buildingQuestions,
        String buildingName) {
        this.buildingWalls= buildingWalls;
        this.buildingQuestions= buildingQuestions;
        this.buildingName= buildingName;
        height= DEFAULT_BUILDING_HEIGHT;
        buildpaper= null;
        visible= true;
    }

    /** @objetivo: calcula el valor del upperLeftPoint del building
     * @precondiciones: el building ya se ha creado
     * @poscondiciones: el valor de upperLeftPoint se ha calculado y asignado
     * @autor: Emmanuel
     * @fecha: 28/marzo/2019 */
    public void upperLeftPointSetter() {
        if (upperLeftPoint == null) {
            int smallestX= buildingWalls.get(0).getCoordinate1().getX();
            int smallestY= buildingWalls.get(0).getCoordinate1().getY();
            Coordinate upperLeftPoint= new Coordinate();

            for (Wall w : buildingWalls) {
                if (w.getCoordinate1().getX() < smallestX) smallestX= w.getCoordinate1().getX();
                if (w.getCoordinate2().getX() < smallestX) smallestX= w.getCoordinate2().getX();
                if (w.getCoordinate1().getY() < smallestY) smallestY= w.getCoordinate1().getY();
                if (w.getCoordinate2().getY() < smallestY) smallestY= w.getCoordinate2().getY();
            }

            upperLeftPoint.setX(smallestX);
            upperLeftPoint.setY(smallestY);

            setUpperLeftPoint(upperLeftPoint);
        }
    }

    /** @objetivo: calcula el valor del bottomRightPoint del building
     * @precondiciones: el building ya se ha creado
     * @poscondiciones: el valor de bottomRightPoint se ha calculado y asignado
     * @autor: Emmanuel
     * @fecha: 28/marzo/2019 */
    public void bottomRightPointSetter() {
        if (bottomRightPoint == null) {
            int largestX= buildingWalls.get(0).getCoordinate1().getX();
            int largestY= buildingWalls.get(0).getCoordinate1().getY();
            Coordinate bottomRightPoint= new Coordinate();

            for (Wall w : buildingWalls) {
                if (w.getCoordinate1().getX() > largestX) largestX= w.getCoordinate1().getX();
                if (w.getCoordinate2().getX() > largestX) largestX= w.getCoordinate2().getX();
                if (w.getCoordinate1().getY() > largestY) largestY= w.getCoordinate1().getY();
                if (w.getCoordinate2().getY() > largestY) largestY= w.getCoordinate2().getY();
            }

            bottomRightPoint.setX(largestX);
            bottomRightPoint.setY(largestY);

            setBottomRightPoint(bottomRightPoint);
        }
    }

    // Getters and Setters
    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height= height;
    }

    public String getBuildingDirectory() {
        return buildingDirectory;
    }

    public void setBuildingDirectory(String buildingDirectory) {
        this.buildingDirectory= buildingDirectory;
    }

    public File getBuildpaper() {
        return buildpaper;
    }

    public void setBuildpaper(File buildpaper) {
        this.buildpaper= buildpaper;
    }

    public Wall getWall1() {
        return wall1;
    }

    public void setWall1(Wall wall1) {
        this.wall1= wall1;
    }

    public Wall getWall2() {
        return wall2;
    }

    public void setWall2(Wall wall2) {
        this.wall2= wall2;
    }

    public Wall getWall3() {
        return wall3;
    }

    public void setWall3(Wall wall3) {
        this.wall3= wall3;
    }

    public Wall getWall4() {
        return wall4;
    }

    public void setWall4(Wall wall4) {
        this.wall4= wall4;
    }

    public ArrayList<Wall> getBuildingWalls() {
        return buildingWalls;
    }

    public ArrayList<Question> getBuildingQuestions() {
        return buildingQuestions;
    }

    public ArrayList<Wall> getBuildingWall() {
        return buildingWalls;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingWalls(ArrayList<Wall> buildingWalls) {
        this.buildingWalls= buildingWalls;
    }

    public BufferedImage getBuilding2DImage() throws IOException {
        return ImageIO.read(building2DImage);
    }

    public File getBuildingImage() {
        return building2DImage;
    }

    public void setBuilding2DImage(File building2dImage) {
        building2DImage= building2dImage;
    }

    public Coordinate getUpperLeftPoint() {
        return upperLeftPoint;
    }

    public void setUpperLeftPoint(Coordinate upperLeftPoint) {
        this.upperLeftPoint= upperLeftPoint;
    }

    public Coordinate getBottomRightPoint() {
        return bottomRightPoint;
    }

    public void setBottomRightPoint(Coordinate bottomRightPoint) {
        this.bottomRightPoint= bottomRightPoint;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible= visible;
    }

    public Rectangle getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rectangle hitbox) {
        this.hitbox= hitbox;
    }

    public Rectangle getProximityBox() {
        return proximityBox;
    }

    public void setProximityBox(Rectangle proximityBox) {
        this.proximityBox= proximityBox;
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close= close;
    }

}
