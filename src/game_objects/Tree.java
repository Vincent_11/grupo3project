package game_objects;

import java.awt.Graphics;

import gfx.ImageLoader;
import gfx.SpriteSheet;
import plane.Coordinate;

public abstract class Tree {

    protected static final int tWidth= 60, tHeight= 60;
    protected static final SpriteSheet trees= new SpriteSheet(
        ImageLoader.loadImage("/textures/trees_sheet.png"));
    protected Coordinate position;

    public Tree(Coordinate position) {
        this.position= position;
    }

    public Tree(int x, int y) {
        position= new Coordinate(x, y);
    }

    public Coordinate getPosition() {
        return position;
    }

    /** @author Vincent Prado
     * @param g         Graphic context
     * @param xPosition the tree's horizontal position
     * @param yPosition the tree's vertical position
     * @param width     the image width
     * @param height    the image height
     * @date 3/19/2020 This abstract method to draw trees. */
    public abstract void draw(Graphics g, int xPosition, int yPosition, int width, int height);
}
