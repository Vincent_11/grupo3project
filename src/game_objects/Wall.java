package game_objects;

import java.io.File;

import plane.Coordinate;

public class Wall {
    private static final int DEFAULT_WALL_HEIGHT= 3;
    private Coordinate coordinate1;
    private Coordinate coordinate2;

    private int height;
    private String wallpaperDirectory;
    private File wallpaper;
    private boolean visible;

    public Wall(Coordinate coordinate1, Coordinate coordinate2) {
        this.coordinate1= coordinate1;
        this.coordinate2= coordinate2;
        height= DEFAULT_WALL_HEIGHT;
        wallpaper= null;
        visible = true;
    }

    /** Objetivo: crear un objeto Wall Precondiciones: All fields are entered Postcondiciones: two
     * coordinates and one file are created Argumentos: Autor: Emmanuel Hernandez Fecha:
     * 8/marzo/2020 */
    public Wall(int x1, int y1, int x2, int y2, int height, String wallpaperDirectory) {
        coordinate1= new Coordinate(x1, y1);
        coordinate2= new Coordinate(x2, y2);
        this.height= height;
        wallpaper= new File(wallpaperDirectory); // saved as File for unfamiliarity with VRML will
                                                 // alter if needed
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height= height;
    }

    public File getWallpaper() {
        return wallpaper;
    }

    public void setWallpaper(File image) {
        wallpaper= image;
    }

    public Coordinate getCoordinate1() {
        return coordinate1;
    }

    public Coordinate getCoordinate2() {
        return coordinate2;
    }

    public String getWallpaperDirectory() {
        return wallpaperDirectory;
    }

    public void setWallpaperDirectory(String wallpaperDirectory) {
        this.wallpaperDirectory= wallpaperDirectory;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
