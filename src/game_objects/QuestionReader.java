package game_objects;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class QuestionReader {
    private static ArrayList<Question> MATElist;
    private static ArrayList<Question> ICOMlist;
    private static ArrayList<Question> INELlist;
    private static ArrayList<Question> INQUlist;
    private static ArrayList<Question> CULTlist;

    public QuestionReader() {
        MATElist= new ArrayList<>();
        ICOMlist= new ArrayList<>();
        INELlist= new ArrayList<>();
        INQUlist= new ArrayList<>();
        CULTlist= new ArrayList<>();
        fillList();
    }

    /** @author Ralph Ulysse
     * 
     * @date 04/05/2020 reads the file (bank of questions) and sort each question to his respective
     *       theme */
    private void fillList() {
        String csvFile= "res/csv/Questions.csv";
        BufferedReader br= null;
        String line= "";
        String cvsSplitBy= "\t";

        try {
            br= new BufferedReader(new FileReader(csvFile));
            int counter= 1;
            while ((line= br.readLine()) != null && line.length() != 0) {
                if (counter <= 10) {
                    String[] questions= line.split(cvsSplitBy);
                    INQUlist.add(new Question(questions[0], questions[1], questions[2],
                        questions[3], questions[4], questions[5]));
                } else if (counter > 10 && counter <= 20) {
                    String[] questions= line.split(cvsSplitBy);
                    ICOMlist.add(new Question(questions[0], questions[1], questions[2],
                        questions[3], questions[4], questions[5]));
                } else if (counter > 20 && counter <= 30) {
                    String[] questions= line.split(cvsSplitBy);
                    CULTlist.add(new Question(questions[0], questions[1], questions[2],
                        questions[3], questions[4], questions[5]));
                } else if (counter > 30 && counter <= 40) {
                    String[] questions= line.split(cvsSplitBy);
                    MATElist.add(new Question(questions[0], questions[1], questions[2],
                        questions[3], questions[4], questions[5]));
                } else {
                    String[] questions= line.split(cvsSplitBy);
                    INELlist.add(new Question(questions[0], questions[1], questions[2],
                        questions[3], questions[4], questions[5]));
                }
                counter++ ;

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /** @author Ralph Ulysse
     * 
     * @date 04/08/2020 the methods generate generates random question out of the options available
     *       and the methods unused adds the rest in a list so they can be sorted later */

    public Question generateMATEQuestion() {
        Random rand= new Random();
        int index= rand.nextInt(MATElist.size());
        return MATElist.remove(index);
    }

    public void addUnusedMATEQuestions(ArrayList<Question> questions) {
        MATElist.addAll(questions);
    }

    public Question generateICOMQuestion() {
        Random rand= new Random();
        int index= rand.nextInt(ICOMlist.size());
        return ICOMlist.remove(index);
    }

    public void addUnusedICOMQuestions(ArrayList<Question> questions) {
        ICOMlist.addAll(questions);
    }

    public Question generateINELQuestion() {
        Random rand= new Random();
        int index= rand.nextInt(INELlist.size());
        return INELlist.remove(index);
    }

    public void addUnusedINELQuestions(ArrayList<Question> questions) {
        INELlist.addAll(questions);
    }

    public Question generateCULTQuestion() {
        Random rand= new Random();
        int index= rand.nextInt(CULTlist.size());
        return CULTlist.remove(index);
    }

    public void addUnusedCULTQuestions(ArrayList<Question> questions) {
        CULTlist.addAll(questions);
    }

    public Question generateINQUQuestion() {
        Random rand= new Random();
        int index= rand.nextInt(INQUlist.size());
        return INQUlist.remove(index);
    }

    public void addUnusedINQUQuestions(ArrayList<Question> questions) {
        INQUlist.addAll(questions);
    }

}