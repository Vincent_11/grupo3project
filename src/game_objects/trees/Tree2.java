package game_objects.trees;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import game_objects.Tree;
import plane.Coordinate;

public class Tree2 extends Tree {

    private static final BufferedImage image= trees.crop(tWidth * 3 + 1, tHeight, tWidth, tHeight);

    public Tree2(Coordinate position) {
        super(position);
    }

    public Tree2(int x, int y) {
        super(x, y);
    }

    /** @author Vincent Prado
     * @param g         Graphic context
     * @param xPosition the tree's horizontal position
     * @param yPosition the tree's vertical position
     * @param width     the image width
     * @param height    the image height
     * @date 3/19/2020 draws the corresponding tree image */
    @Override
    public void draw(Graphics g, int xPosition, int yPosition, int width, int height) {
        g.drawImage(image, xPosition, yPosition, width, height, null);
    }
}
