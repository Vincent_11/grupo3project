package game_objects;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Manager.DevelopmentManager;

public class BuildingFinishingTouches {

    public void buildingNameAndTrivia() {
        String name= JOptionPane.showInputDialog("Please Enter Building Name");

        JFrame f= new JFrame("TRIVIA");
        f.setLayout(new GridLayout(1, 1));
        f.setSize(800, 400);
        f.setDefaultCloseOperation(f.DISPOSE_ON_CLOSE);
        JPanel panel= new JPanel(new GridBagLayout());
        GridBagConstraints x= new GridBagConstraints();
        x.insets= new Insets(10, 10, 10, 10);

        // Buttons
        boolean[] chosenFlags= new boolean[5];

        Button a= new Button("Maths Questions");
        Button b= new Button("Computer Engineering Questions");
        Button c= new Button("Electrical Engineering Questions");
        Button d= new Button("Chemical Engineering Questions");
        Button e= new Button("Cultural Questions");
        a.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                a.setBackground(Color.GREEN);
                b.setBackground(Color.WHITE);
                c.setBackground(Color.WHITE);
                d.setBackground(Color.WHITE);
                e.setBackground(Color.WHITE);
                checkFlags(chosenFlags, 0);
            }
        });
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                b.setBackground(Color.GREEN);
                a.setBackground(Color.WHITE);
                c.setBackground(Color.WHITE);
                d.setBackground(Color.WHITE);
                e.setBackground(Color.WHITE);
                checkFlags(chosenFlags, 1);
            }
        });
        c.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                c.setBackground(Color.GREEN);
                a.setBackground(Color.WHITE);
                b.setBackground(Color.WHITE);
                d.setBackground(Color.WHITE);
                e.setBackground(Color.WHITE);
                checkFlags(chosenFlags, 2);
            }
        });
        d.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                d.setBackground(Color.GREEN);
                a.setBackground(Color.WHITE);
                b.setBackground(Color.WHITE);
                c.setBackground(Color.WHITE);
                e.setBackground(Color.WHITE);
                checkFlags(chosenFlags, 3);
            }
        });
        e.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                e.setBackground(Color.GREEN);
                b.setBackground(Color.WHITE);
                c.setBackground(Color.WHITE);
                d.setBackground(Color.WHITE);
                a.setBackground(Color.WHITE);
                checkFlags(chosenFlags, 4);
            }
        });
        Button ok= new Button("Add Questions to Building");
        ok.setSize(100, 50);
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                QuestionReader reader= new QuestionReader();
                if (chosenFlags[0]) {
                    ArrayList<Question> questions= new ArrayList<>();
                    for (int i= 0; i < 4; i++ ) {
                        questions.add(reader.generateMATEQuestion());
                    }
                    DevelopmentManager.addBuilding(f, questions, name);
                } else if (chosenFlags[1]) {
                    ArrayList<Question> questions= new ArrayList<>();
                    for (int i= 0; i < 4; i++ ) {
                        questions.add(reader.generateICOMQuestion());
                    }
                    DevelopmentManager.addBuilding(f, questions, name);
                } else if (chosenFlags[2]) {
                    ArrayList<Question> questions= new ArrayList<>();
                    for (int i= 0; i < 4; i++ ) {
                        questions.add(reader.generateINELQuestion());
                    }
                    DevelopmentManager.addBuilding(f, questions, name);
                } else if (chosenFlags[3]) {
                    ArrayList<Question> questions= new ArrayList<>();
                    for (int i= 0; i < 4; i++ ) {
                        questions.add(reader.generateINQUQuestion());
                    }
                    DevelopmentManager.addBuilding(f, questions, name);
                } else {
                    ArrayList<Question> questions= new ArrayList<>();
                    for (int i= 0; i < 4; i++ ) {
                        questions.add(reader.generateCULTQuestion());
                    }
                    DevelopmentManager.addBuilding(f, questions, name);
                }
            }
        });

        x.gridx= 0;
        x.gridy= 1;
        panel.add(a, x);
        x.gridx= 0;
        x.gridy= 2;
        panel.add(b, x);
        x.gridx= 0;
        x.gridy= 3;
        panel.add(c, x);
        x.gridx= 0;
        x.gridy= 4;
        panel.add(d, x);
        x.gridx= 0;
        x.gridy= 5;
        panel.add(e, x);
        x.gridx= 0;
        x.gridy= 6;
        panel.add(ok, x);

        JPanel border= new JPanel(new BorderLayout());
        JLabel k= new JLabel("PLEASE SELECT A THEME FOR YOUR BUILDING:");
        border.add(k);
        panel.add(border);
        f.add(panel, BorderLayout.WEST);
        f.setLocationRelativeTo(null);
        f.setVisible(true);

    }

    private void checkFlags(boolean[] flags, int index) {
        for (int i= 0; i < flags.length; i++ ) {
            if (i == index)
                flags[i]= true;
            else
                flags[i]= false;
        }
    }

}
