package game_objects;

public class Question {

    private String question;
    private String possibleAnswerA;
    private String possibleAnswerB;
    private String possibleAnswerC;
    private String possibleAnswerD;
    private String answer;

    public Question(String q, String a, String b, String c, String d, String right) {
        question= q;
        possibleAnswerA= a;
        possibleAnswerB= b;
        possibleAnswerC= c;
        possibleAnswerD= d;
        answer= right;
    }

    /** @author Ralph Ulysse Display each questions with his respective answers possibilities. with
     *         the correct answer being the first one. method is designed to be displayed in
     *         question text file */
    @Override
    public String toString() {
        if (this.answer.equalsIgnoreCase(this.possibleAnswerA)) {
            return this.question +
                "\nRight: " + this.answer +
                "\nWrong: " + this.possibleAnswerB +
                "\nWrong: " + this.possibleAnswerC +
                "\nWrong: " + this.possibleAnswerD;
        } else if (this.answer.equalsIgnoreCase(this.possibleAnswerB)) {
            return this.question +
                "\nRight: " + this.answer +
                "\nWrong: " + this.possibleAnswerA +
                "\nWrong: " + this.possibleAnswerC +
                "\nWrong: " + this.possibleAnswerD;
        } else if (this.answer.equalsIgnoreCase(this.possibleAnswerC)) {
            return this.question +
                "\nRight: " + this.answer +
                "\nWrong: " + this.possibleAnswerA +
                "\nWrong: " + this.possibleAnswerB +
                "\nWrong: " + this.possibleAnswerD;
        } else {
            return this.question +
                "\nRight: " + this.answer +
                "\nWrong: " + this.possibleAnswerA +
                "\nWrong: " + this.possibleAnswerB +
                "\nWrong: " + this.possibleAnswerC;
        }
    }

    public String getQuestion() {
        return question;
    }

    public String getPossibleAnswerA() {
        return "a) " + possibleAnswerA;
    }

    public String getPossibleAnswerB() {
        return "b) " + possibleAnswerB;
    }

    public String getPossibleAnswerC() {
        return "c) " + possibleAnswerC;
    }

    public String getPossibleAnswerD() {
        return "d) " + possibleAnswerD;
    }

    public String getAnswer() {
        return answer;
    }

    public boolean isAnswer(String possibleAnswer) {
        return answer.equals(possibleAnswer.substring(3));
    }

}