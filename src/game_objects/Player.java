package game_objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import gfx.ImageLoader;
import gfx.SpriteSheet;

public class Player {
    private static final int WIDHT= 40, HEIGTH= 40;
    private static final SpriteSheet player= new SpriteSheet(
        ImageLoader.loadImage("/textures/player_sheet.png"));
    private static final BufferedImage image= player.crop(0, 0, WIDHT, HEIGTH);
    private int xPosition, yPosition;
    private Rectangle playerHitbox;

    public Player(int xPosition, int yPosition) {
        this.xPosition= xPosition;
        this.yPosition= yPosition;
        playerHitbox= new Rectangle(xPosition, yPosition, 20, 20);
    }

    /** @author Vincent Prado
     * @param g         graphic context
     * @param xPosition the player's horizontal position
     * @param yPosition the players's vertical position
     * @date 03/29/2020 draws the player in the position specified by xPosition and yPosition */
    public void draw(Graphics g) {
        g.drawImage(image, xPosition, yPosition, 20, 20, null);
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition= xPosition;
        playerHitbox.setLocation(xPosition, yPosition);
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition= yPosition;
        playerHitbox.setLocation(xPosition, yPosition);
    }

    public Rectangle getPlayerHitbox() {
        return playerHitbox;
    }

}
