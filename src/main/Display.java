package main;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import Manager.GameManager;
import Manager.StateChangerManager;
import draw.DrawWallListener;
import draw.KeyManager;

public class Display extends JPanel {

    private JLabel cursorPosition;

    public Display(int width, int height) {
//        Todo eliminar
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("UP"), "move up");
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("DOWN"), "move down");
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("RIGHT"), "move right");
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("LEFT"), "move left");
        getActionMap().put("move up", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameManager.up();
            }
        });

        getActionMap().put("move down", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameManager.down();
            }
        });

        getActionMap().put("move right", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameManager.right();
            }
        });

        getActionMap().put("move left", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameManager.left();
            }
        });
        setLayout(null);
        setFocusTraversalKeysEnabled(false);
        setFocusable(true);
        requestFocus();
        setPreferredSize(new Dimension(width, height));
        addMouseListener(new DrawWallListener(this));
        addMouseMotionListener(new DrawWallListener(this));
        addKeyListener(new KeyManager(this));
        cursorPosition= new JLabel("X = " + "," + "Y = ");
        add(cursorPosition);
    }

    /** @author: Vincent Prado
     * @param: xPosition the cursor's horizontal position
     * @param: yPosition the cursor's vertical position
     * @date 3/6/2020 Shows the cursor position on top of the cursor. */
    public void setCursorPosition(int xPosition, int yPosition) {
        cursorPosition.setText("X = " + xPosition + ", " + "Y = " + yPosition);
        cursorPosition.setBounds(xPosition - 40, yPosition - 40, 100, 50);
        repaint();
    }

    @Override
    /** @author: Vincent Prado
     * @date 3/4/2020 Draw walls stored in the list. */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        try {
            StateChangerManager.drawState(g);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
