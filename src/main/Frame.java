package main;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import Manager.DevelopmentManager;
import Manager.GameManager;
import Manager.StateChangerManager;
import game_objects.BuildingFinishingTouches;

public class Frame {

    private JFrame frame;

    private String title;
    private int width, height;
    DevelopmentManager developmentManager;
    GameManager gameManager;

    public Frame(String title, int width, int height) {
        this.title= title;
        this.width= width;
        this.height= height;

        createDisplay();
    }

    private void createDisplay() {
        frame= new JFrame(title);
        frame.setSize(width, height);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());
        StateChangerManager stateChangerManager = new StateChangerManager(width - 234, height - 132, frame);
    }

    public DevelopmentManager getManager() {
        return developmentManager;
    }

    public JFrame getFrame() {
        return frame;
    }
}
