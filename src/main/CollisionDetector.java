package main;

import java.awt.Rectangle;
import java.util.ArrayList;

import game_objects.Building;
import game_objects.Player;
import plane.Coordinate;

public class CollisionDetector {

    Player player;
    ArrayList<Rectangle> hitboxList;

    public CollisionDetector(Player player) {
        super();
        this.player= player;
        hitboxList= new ArrayList<>();
    }

    /** @objetivo: verifica si el jugador est� cerca o si ha interactuado con un edificio
     * @precondiciones: existe por lo menos un building
     * @poscondiciones: se altera el valor del valor boolean close del building
     * @param player    el jugador
     * @param buildings la lista de los edificios
     * @autor: Emmanuel
     * @fecha:12/abril/2020 */
    public void buildingProximityDetector(Player player, ArrayList<Building> buildings) {
        // checks building proximity
        for (Building building : buildings) {
            if (player.getPlayerHitbox().getMinX() <= building.getProximityBox().getMaxX() &&
                player.getPlayerHitbox().getMaxX() >= building.getProximityBox().getMinX() &&
                player.getPlayerHitbox().getMinY() <= building.getProximityBox().getMaxY() &&
                player.getPlayerHitbox().getMaxY() >= building.getProximityBox().getMinY()) {

                building.setClose(true);
            } else {
                building.setClose(false);
            }
        }

        // checks building collision
        for (Building building : buildings) {
            if (player.getPlayerHitbox().getMinX() <= building.getHitbox().getMaxX() &&
                player.getPlayerHitbox().getMaxX() >= building.getHitbox().getMinX() &&
                player.getPlayerHitbox().getMinY() <= building.getHitbox().getMaxY() &&
                player.getPlayerHitbox().getMaxY() >= building.getHitbox().getMinY()) {

                System.out.println("Player has hit Building");
                // add question menu code here
            }
        }
    }

    /** @objetivo: crea hitboxes para cualquier building que no tenga uno
     * @precondiciones: hay buildings en la lista buildings sin hitbox
     * @poscondiciones: se la ha asignado un hitbox a los buildings
     * @param buildings la lista de los edificios
     * @autor: Emmanuel
     * @fecha:12/abril/2020 */
    public void setBuildingHitboxes(ArrayList<Building> buildings) {
        for (Building b : buildings) {
            if (b.getHitbox() == null) {
                int delta= 60;      // diferecial de proximityBox y hitbox de un building
                Coordinate upperLeftPoint= b.getUpperLeftPoint();
                Coordinate bottomRightPoint= b.getBottomRightPoint();

                // crea un rectangle y se lo asigna al building b como su hitbox
                Rectangle hitbox= new Rectangle(upperLeftPoint.getX(),
                    upperLeftPoint.getY(),
                    bottomRightPoint.getX() - upperLeftPoint.getX(),
                    bottomRightPoint.getY() - upperLeftPoint.getY());

                b.setHitbox(hitbox);

                // crea un rectangle y se lo asigna al building b como su proximity box
                Coordinate proximityUpperLeftPoint= new Coordinate(0, 0);
                Coordinate proximityBottomRightPoint= new Coordinate(0, 0);

                proximityUpperLeftPoint.setX(upperLeftPoint.getX() - delta);
                proximityUpperLeftPoint.setY(upperLeftPoint.getY() - delta);

                proximityBottomRightPoint.setX(bottomRightPoint.getX() + delta);
                proximityBottomRightPoint.setY(bottomRightPoint.getY() + delta);

                Rectangle proximityBox= new Rectangle(proximityUpperLeftPoint.getX(),
                    proximityUpperLeftPoint.getY(),
                    proximityBottomRightPoint.getX() - proximityUpperLeftPoint.getX(),
                    proximityBottomRightPoint.getY() - proximityUpperLeftPoint.getY());

                b.setProximityBox(proximityBox);
            }
        }
    }
}
