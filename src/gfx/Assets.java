package gfx;

import java.awt.image.BufferedImage;

public class Assets {

    private static final int tWidth= 60, tHeight= 60;
    private static final SpriteSheet trees= new SpriteSheet(
        ImageLoader.loadImage("/textures/trees_sheet.png"));
    private static BufferedImage tree1= trees.crop(tWidth * 6, tHeight * 4, tWidth, tHeight);

//    public Assets(){
//        SpriteSheet trees = new SpriteSheet(ImageLoader.loadImage("/textures/trees_sheet.png"));
//        tree1 = trees.crop(tWidth * 6, tHeight * 4, tWidth, tHeight);
//    }

    public static BufferedImage getTree1() {
        return tree1;
    }
}
