package gfx;

import java.awt.image.BufferedImage;

public class SpriteSheet {
    private BufferedImage sheet;

    public SpriteSheet(BufferedImage sheet) {
        this.sheet= sheet;
    }

    /** @param x  horizontal position to start cropping
     * @param y      vertical position to start cropping
     * @param width  width of the cropped image
     * @param height height of the cropped image
     * @return cropped buffered image This method is use to crop images. It is useful when using
     *         sprite sheets */
    public BufferedImage crop(int x, int y, int width, int height) {
        return sheet.getSubimage(x, y, width, height);
    }
}
